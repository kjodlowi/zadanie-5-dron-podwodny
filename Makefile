#
#  To sa opcje dla kompilacji
#
CPPFLAGS= -c -g -Iinc -Wall -pedantic

__start__: drone
	./drone

drone: obj/main.o obj/gnuplot_link.o 
	g++ -Wall -pedantic -o drone obj/main.o \
                                  obj/gnuplot_link.o

obj/main.o: src/main.cpp inc/gnuplot_link.hh inc/Shape.hh inc/Shapes.hh inc/Matrix.hh inc/Vector.hh
	g++ ${CPPFLAGS} -o obj/main.o src/main.cpp

obj/gnuplot_link.o: src/gnuplot_link.cpp inc/gnuplot_link.hh
	g++ ${CPPFLAGS} -o obj/gnuplot_link.o src/gnuplot_link.cpp


clean:
	rm -f obj/*.o drone
