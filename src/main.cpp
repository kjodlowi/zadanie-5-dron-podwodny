#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <thread>
#include <chrono>
#include <list>

#include "gnuplot_link.hh"
#include "Shapes.hh"

using namespace std;

template<>
int Vector3D::all_vectors = 0;

template<>
int Vector3D::destroyed_vectors = 0;

int main()
{
    PzG::GnuplotLink link;

    Vector3D translation;

    string tmp = "solid/tmp/tmp.dat";
    string tmprism1 = "solid/tmp/tmprism1.dat";
    string tmprism2 = "solid/tmp/tmprism2.dat";
    string tmplain = "solid/tmp/tmplain.dat";
    string rod1tmp = "solid/tmp/rod1tmp.dat";
    string cuboidtmp = "solid/tmp/cuboidtmp.dat";
    string water = "solid/water.dat";
    string ground = "solid/bottom.dat";

    Drone drone;

    list<shared_ptr<base_Shape>> obstacles;
    list<shared_ptr<base_Shape>>::iterator it;

    obstacles.push_back(make_shared<Rod>());
    obstacles.push_back(make_shared<Cuboid>());
    obstacles.push_back(make_shared<Plain>());
    obstacles.push_back(make_shared<Water>());
    obstacles.push_back(make_shared<Ground>());

    it = obstacles.begin();
    (*it)->init(rod1tmp,100,-150,-50,90);
    it++;
    (*it)->init(cuboidtmp,150,150,-200,0);
    it++;
    (*it)->init(tmplain,-100,250,-100,0);
    it++;
    (*it)->init(water,0,0,0,0);
    it++;
    (*it)->init(ground,0,0,0,0);

    link.Init();

    link.SetDrawingMode(PzG::DM_3D);

    link.AddFilename(tmp.c_str(), PzG::LS_CONTINUOUS, 1);
    link.AddFilename(tmplain.c_str(), PzG::LS_CONTINUOUS, 1);
    link.AddFilename(tmprism1.c_str(), PzG::LS_CONTINUOUS, 1);
    link.AddFilename(tmprism2.c_str(), PzG::LS_CONTINUOUS, 1);
    link.AddFilename(water.c_str(), PzG::LS_CONTINUOUS, 1);
    link.AddFilename(ground.c_str(), PzG::LS_CONTINUOUS, 3);
    link.AddFilename(rod1tmp.c_str(), PzG::LS_CONTINUOUS, 1);
    link.AddFilename(cuboidtmp.c_str(), PzG::LS_CONTINUOUS, 1);
    //link.AddFilename("solid/tmp/hitbox.dat", PzG::LS_CONTINUOUS, 1);


    int s = 0;
    double r,d;

    cout << endl
        << " Wybierz tryb dzialania programu:  " << endl
        << "         1.Normalny " << endl
        << "         2.Modyfikacja " << endl
        << "Twoj wybor: ";
    cin >> s; 

    if (s == 2)
    {
        s = 1;
    }else
    {
        s = 0;
    }
    

    drone.init(s);
    drone.Draw();
    
    link.Draw();
    
    s = 0;

    while (s != 3)
    {
        switch (s)
        {
        case 1:
            cout << endl
                 << "Wprowadz kat obrotu: ";
            cin >> r;
            r = r/FRAMES;
            for (int i = 0; i < FRAMES; i++)
            {
                drone.turn(r);
                drone.Draw();
                link.Draw();
                std::this_thread::sleep_for(std::chrono::milliseconds(10));
            }
            s = 0;
            break;
        case 2:
            cout << "Wprowadz odleglosc: ";
            cin  >> d;
            cout << endl
                 << "Wprowadz kat przemieszczenia w pionie: ";
            cin  >> r;

            translation = drone.moveVector(d,r) / FRAMES;
        
            for (int i = 0; i < FRAMES; i++)
            {
                drone.move(translation);
                drone.getHitbox().findMinMax();

                for (it = obstacles.begin(); it != obstacles.end(); it++)
                {
                    (*it)->findMinMax();
                    
                    if ( (*it)->isCollision(drone.getHitbox()) ) 
                    {
                        cout << "Dron za bardzo zblizyl sie do obiektu: " << (*it)->getName() << endl
                             << "Wcisnij ENTER" << endl;
                        cin.ignore(10000,'\n');
                        cin.ignore(10000,'\n');

                        return 0;
                    }
                }          

                drone.Draw();
                link.Draw();

                std::this_thread::sleep_for(std::chrono::milliseconds(10));
            }
            s = 0;
            break;
        default:
            cout << endl
                 << endl
                 << "   ---------------------" << endl
                 << "    Wybierz operacje: " << endl
                 << "     1. Obrot " << endl
                 << "     2. Przesuniecie " << endl
                 << "     3. Koniec" << endl
                 << "   ---------------------" << endl
                 << "   Liczba lacznie utworzonych wektorow: " << Vector3D::totalObjects() << endl
                 << "   Liczba aktualnie istniejacych wektorow: " << Vector3D::liveObjects() << endl
                 << endl
                 << "     Twoj wybor: ";
            cin >> s;
            break;
        }
    }


}