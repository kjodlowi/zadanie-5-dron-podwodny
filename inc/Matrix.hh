#pragma once

#include "Vector.hh"
#include <iostream>
#include <iomanip>

/**
 * @brief Klasa Matrix<typ,rozmiar> pozwala na utworzenie macierzy kwadratowej na tablicy wektorow, o dowolnym typie i rozmiarze.
 * 
 * @tparam Type 
 * @tparam Size 
 */
template <typename Type, int Size>
class Matrix
{
  Vector<Type, Size> _tab[Size];
public:

  /** @brief metoda, która zwraca wartosc przypisana do elementu [n][m] macierzy 
  */
  Type operator()(int n, int m) const { return _tab[n](m); }

  /** @brief metoda, która zamienia wartosc elementu [n][m] macierzy 
  */
  Type & operator()(int n, int m)  { return _tab[n](m);  }


};

/**
 * @brief Operator zapisywania macierzy odczytujac dane ze strumienia
 *
 * @tparam Type 
 * @tparam Size 
 * @param stream 
 * @param matrix 
 * @return std::istream& 
 */
template <typename Type, int Size>
std::istream &operator>>(std::istream &stream, Matrix<Type, Size> &matrix)
{
    for (int i=0;i < Size;i++)
    {
        for (int j = 0; j < Size;j++)
        {
           stream >> matrix(i,j);
        }
    }
    return stream;
}

/**
 * @brief Operator wypisywania macierzy odczytujac dane ze strumienia
 */
template <typename Type, int Size>
std::ostream &operator<<(std::ostream &stream, const Matrix<Type, Size> &matrix)
{
    for (int i=0;i < Size;i++)
    {
        for (int j = 0; j < Size;j++)
        {
            stream << matrix(j,i) << "  ";
        }
        stream << std::endl;
    }
    return stream;    
}


/**
 * @brief Zwraca wyznacznik macierzy
 * 
 * @tparam Type 
 * @tparam Size 
 * @param matrix 
 * @return Type wyznacznik macierzy o typie danych, z których macierz się składa
 */
template <typename Type, int Size>
Type determinant(Matrix<Type, Size> matrix)
{
    int n = 0, m = 0, j = 1;
    Type tmp;

    do 
    {
        if (matrix(n,n) != 0)
        {
            j = 1;

            while (n + j < Size)
            {
                tmp = matrix(n+j,n) / matrix(n,n);
                while (m < Size)
                {
                    matrix(n+j,m) -= matrix(n,m) * tmp;
                    m++;
                }
                m = 0;
                j++;
            }
            n++;
            j = 1;
            m = 0;
        }else
        {
            while (m < Size)
            {
                tmp = matrix(n,m);
                matrix(n,m) = matrix(n+j,m);
                matrix(n+j,m) = tmp;
                m++;
            }
            j++;
            if (n+j >= Size)
            {
                if (matrix(n,n) != 0)
                {
                    n++;
                }else
                {
                    tmp = 0;
                    n = Size + 1;
                }
                
            }
        }
    }while (n < Size);

    tmp = 1;
    n = -1;
    while (n < Size -1)
    {
       n++; 
       tmp *= matrix(n,n);
    }

    return tmp;
}

/**
 * @brief Operator mnożenia macierzy przez wektor
 * 
 * @tparam Type 
 * @tparam Size 
 * @param matrix 
 * @param vec 
 * @return Vector<Type, Size> zwraca wektor, który jest wynikiem operacji mnożenia macierzy przez wektor
 */
template <typename Type, int Size>
Vector<Type, Size> operator*(const Matrix<Type, Size> matrix, const Vector<Type, Size> vec)
{
    Vector<Type, Size> result;
    Type tmp(0);

    for (int i = 0; i < Size; i++)
    {
        tmp = 0;
        for (int j = 0; j < Size; j++)
        {
            tmp += matrix(i,j) * vec(j);
        }
        result(i) = tmp;
    }
    return result;
}

/**
 * @brief Operator mnożenia macierzy
 * 
 * @tparam Type 
 * @tparam Size 
 * @param matrix1 
 * @param matrix2 
 * @return Matrix<Type, Size> 
 */
template <typename Type, int Size>
Matrix<Type, Size> operator*(const Matrix<Type, Size> &matrix1, const Matrix<Type, Size> &matrix2)
{
    Matrix<Type, Size> result;
    Type tmp;

    for (int i = 0; i < Size; i++)
    {
        for (int j = 0; j < Size; j++)
        {
            tmp = 0;
            for (int k = 0; k < Size; k++)
            {
                tmp += matrix1(i,k) * matrix2(k,j);
            }
            result(i,j) = tmp;
        }
        
    }
    return result;
}