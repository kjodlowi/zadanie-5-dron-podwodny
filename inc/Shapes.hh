#include "Shape.hh"

/**
 * @brief funkcja, ktora zmienia poszczegolne wartosci wektora 3D
 * 
 * @param vec wektor, ktorego wartosci chemy zmienic
 * @param a wartosc x
 * @param b wartosc y
 * @param c wartosc z
 * @return Vector3D 
 */
Vector3D Vec_init(Vector3D & vec, double a, double b, double c)
{
    vec(0) = a;
    vec(1) = b;
    vec(2) = c;

    return vec;
}

/**
 * @brief Klasa, ktora obsluguje hitbox drona ze srubami
 */
class Hitbox : public Shape<8>
{
public:
};

/**
 * @brief Klasa, ktora obsluguje powierzchnie wody
 */
class Water : public Shape<27>
{
public: 
    void init(const std::string file_name, const double a, const double b, const double c, const double arg) override;
    std::string getName() override {return "powierzchnia wody";}
};

/**
 * @brief Funkcja wczytujaca powierzchnie wody
 */
void Water::init(const std::string file_name, const double a, const double b, const double c, const double arg)
{
    this->Read("solid/water.dat");
}


/**
 * @brief Klasa, ktora obsluguje ziemie
 */
class Ground : public Shape<27>
{
public: 
    void init(const std::string file_name, const double a, const double b, const double c, const double arg) override;
    std::string getName() override {return "ziemia";}
};

/**
 * @brief Funkcja wczytujaca ziemie
 */
void Ground::init(const std::string file_name, const double a, const double b, const double c, const double arg)
{
    this->Read("solid/bottom.dat");
}

/**
 * @brief Klasa, która obsługuje prostopadlosciany
 */
class Cuboid : public Shape<20>
{
public:
   virtual void init(const std::string file_name,const double a,const double b,const double c, const double arg) override;
   std::string getName() override {return "Prostopadloscian";}
};

/**
 * @brief Funkcja wczytujaca prostopadloscian
 * 
 * @param file_name nazwa pliku, w ktorym zapisujemy
 * @param a zmienna X polozenia obiektu
 * @param b zmienna Y polozenia obiektu
 * @param c zmienna Y polozenia obiektu
 * @param arg kat obrotu obiektu
 */
void Cuboid::init(const std::string file_name,const double a,const double b,const double c, const double arg)
{
    this->Read("solid/cuboid.dat");
    Vector3D trans;
    Vec_init(trans,a,b,c);
    this->rotateZ(arg);
    this->translate(trans);

    this->Draw(file_name);
}

/**
 * @brief Klasa, ktora obsluguje prety
 * 
 */
class Rod : public Shape<2>
{
public:
   void init(const std::string file_name,const double a,const double b,const double c, const double arg) override;
   std::string getName() override {return "Pret";}
};

/**
 * @brief Funkcja wczytujaca pret
 * 
 * @param file_name nazwa pliku, w ktorym zapisujemy
 * @param a zmienna X polozenia obiektu
 * @param b zmienna Y polozenia obiektu
 * @param c zmienna Z polozenia obiektu
 * @param arg kat obrotu obiektu
 */
void Rod::init(const std::string file_name,const double a,const double b,const double c, const double arg)
{
    this->Read("solid/rod1.dat");
    Vector3D trans;
    Vec_init(trans,a,b,c);
    this->rotateZ(arg);
    this->translate(trans);

    this->Draw(file_name);
}

/**
 * @brief Klasa, ktora obsluguje sciany
 * 
 */
class Plain : public Shape<8>
{

public:
    void init(const std::string file_name,const double a,const double b,const double c, const double arg);
    std::string getName() override {return "Plaszczyzna";}
};

/**
 * @brief Funkcja wczytujaca plaszczyzne
 * 
 * @param file_name nazwa pliku, w ktorym zapisujemy
 * @param a zmienna X polozenia obiektu
 * @param b zmienna Y polozenia obiektu
 * @param c zmienna Y polozenia obiektu
 * @param arg kat obrotu obiektu
 */
void Plain::init(const std::string file_name,const double a,const double b,const double c, const double arg)
{
    this->Read("solid/plain.dat");
    Vector3D trans;
    Vec_init(trans,a,b,c);
    this->rotateZ(arg);
    this->translate(trans);

    this->Draw(file_name);
}

/**
 * @brief Klasa graniastosłup, ktora sluzy jako reprezentacja srub drona.
 * 
 */
class Prism : public Shape<28>
{
};

/**
 * @brief Glowna czesc drona
 * 
 */
class Drone_body : public Cuboid
{ 
};

/**
 * @brief Klasa dron, ktora sklada poszczegolne jego czeci w calosc i zajmuje sie wyswietlaniem jego ruchow.
 * 
 */
class Drone : public Drone_body, public Prism
{
    Drone_body body;
    Prism prism1,prism2;
    Vector3D trans1,trans2;
    Hitbox hitbox;
    bool mode = false; 
public:
    Hitbox & getHitbox(){return hitbox;}
    Vector3D moveVector(const double distance, const double ang);
    void init(bool mod);
    void turn(const double ang);
    void move(const Vector3D translation);
    void Draw();
};

/**
 * @brief Metoda wyliczajaca wektor przesunięcia drona
 * 
 * @param distance dystans przesuniecia
 * @param ang kat przesuniecia
 * @return Vector3D 
 */
Vector3D Drone::moveVector(const double distance, const double ang)
{
    Vector3D vec;
    vec(2) = distance * std::sin(ang * PI/180);
    vec(0) = distance * std::cos(ang * PI/180) * std::sin(body.getAngleZ() * PI/180);
    vec(1) = -distance * std::cos(ang * PI/180) * std::cos(body.getAngleZ() * PI/180);
    return vec;
}

/**
 * @brief Funkcja inicjalizujaca drona
 */
void Drone::init(bool mod)
{
    mode = mod;
    body.Read("solid/drone.dat");
    prism1.Read("solid/prismY.dat");
    prism2.Read("solid/prismY.dat");

    hitbox.Read("solid/hbox.dat");
    hitbox.Draw("solid/tmp/hitbox.dat");
    prism1.scale(15);
    prism2.scale(15);

    trans1(0) = 15; trans1(1) = 25; trans1(2) = 10; 
    trans2(0) = -15; trans2(1) = 25; trans2(2) = 10;   

    prism1.initial_translation(trans1);
    prism2.initial_translation(trans2);
}

/**
 * @brief Funkcja przesuwajaca drona ze srubami o wektor
 * 
 * @param translation wektor translacji
 */
void Drone::move(const Vector3D translation)
{
    body.translate(translation);
    hitbox.translate(translation);
    hitbox.Draw("solid/tmp/hitbox.dat");

    if(mode)
    {
        prism1.rotateX(2);
        prism2.rotateX(2);
    }else
    {
        prism1.rotateY(2);
        prism2.rotateY(2);
    }


    prism1.translate(trans1);
    prism2.translate(trans2);

    prism1.rotateZ_tmp(0);
    prism2.rotateZ_tmp(0);

    prism1.translate(body.getTranslation());
    prism2.translate(body.getTranslation());

}

/**
 * @brief Funkcja obracajaca drona ze srubami
 * 
 * @param ang kat obrotu
 */
void Drone::turn(const double ang)
{   
    body.rotateZ(ang);

    if(mode)
    {
        prism1.rotateX(2);
        prism2.rotateX(2);
    }else
    {
        prism1.rotateY(2);
        prism2.rotateY(2);
    }

    prism1.translate(trans1);
    prism2.translate(trans2);

    prism1.rotateZ_tmp(ang);
    prism2.rotateZ_tmp(ang);

    prism1.translate(body.getTranslation());
    prism2.translate(body.getTranslation());
}

/**
 * @brief Zapisuje aktualne polozenie drona w plikach , co umozliwia jego wyswietlenie w gnuplocie.
 * 
 */
void Drone::Draw()
{
    body.Draw("solid/tmp/tmp.dat");
    prism1.Draw("solid/tmp/tmprism1.dat");
    prism2.Draw("solid/tmp/tmprism2.dat");
}
