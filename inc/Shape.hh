#include <iostream>
#include <fstream>
#include <string>
#include <cmath>

#include "Vector.hh"
#include "Matrix.hh"


#define PI 3.14159265
#define FRAMES 120

using Vector3D = Vector<double, 3>;

/**
 * @brief Klasa bazowa do klasy shape, ktora pozwala uzywac Shared pointerow do przeszod
 * Zawiera wirtualne funkcje, dzieki ktorym shared pointery dzialaja tak, jak powinny
 */
class base_Shape
{
public:
 double Xmin=400, Xmax=-400, Ymin=400, Ymax=-400, Zmin=400, Zmax=-400;
 virtual void init(const std::string file_name,const double a,const double b,const double c, const double arg){std::cout << "tescik" << std::endl;};
 virtual bool isCollision(base_Shape object){return false;};
 virtual void printMinMax(){};
 virtual void findMinMax(){};
 virtual std::string getName(){return "base_shape";};
 virtual void scale(const double scale){};
};


/*!
 * @brief Klasa kształt, która pozwala na stworzenie dowolnego kształtu w przestrzeni 3D
 * 
 * @tparam num_of_points - ilość punktów, jakie ma mieć dany obiekt
 * 
 * @param point[] - tablica wektorów, wzorcowych danego obiektu, każdy wektor jest jednym punktem w przestrzeni
 * @param tmp_point[] - tablica wektorów, z których rysowany jest obiekt
 * @param translation - wektor przechowujacy informacje o przesunieciu obiektu
 * @param angleZ - kąt obrotu względem osi Z
 * @param angleY - kąt obrotu względem osi Y
 */
template <int num_of_points>
class Shape : public base_Shape
{
  Vector3D point[num_of_points];
  Vector3D tmp_point[num_of_points];
  Vector3D translation;
  double angleZ = 0;
  double angleY = 0;
  double angleX = 0;

public:
  void findMinMax();
  /**
   * @brief Zwraca kat obrotu wzgledem osi OZ
  */
 void printMinMax()override{std::cout << Xmin << " " << Xmax << " " << Ymin << " " << Ymax<< " " << Zmin << " " << Zmax << " " << std::endl;}
  double getAngleZ(){return angleZ;}
  /** @brief Zwraca wektor translacji
  */
  Vector3D getTranslation(){return translation;}
  void Read(const std::string file_name);
  void Draw(const std::string file_name) const;
  void translate(const Vector3D &change);
  void initial_translation(const Vector3D &change);
  void rotateZ(const double angle);
  void rotateZ_tmp(const double angle);
  void rotateY(const double angle);
  void rotateX(const double angle);
  void scale(const double scale) override;
  bool isCollision(base_Shape object);
};

/**
 * @brief funkcja, ktora wyszukuje najwieksze maksymalne i minimalne wartosci punktow polozenia ksztaltu
 */
template <int num_of_points>
void Shape<num_of_points>::findMinMax()
{
  for (int i = 0; i < num_of_points; i++)
  {
    if( tmp_point[i](0) < Xmin ) Xmin = tmp_point[i](0);
    if( tmp_point[i](0) > Xmax ) Xmax = tmp_point[i](0);

    if( tmp_point[i](1) < Ymin ) Ymin = tmp_point[i](1);
    if( tmp_point[i](1) > Ymax ) Ymax = tmp_point[i](1);

    if( tmp_point[i](2) < Zmin ) Zmin = tmp_point[i](2);
    if( tmp_point[i](2) > Zmax ) Zmax = tmp_point[i](2);
  }
}

/**
 * @brief Zapisuje kształt do pliku file_name
 * @param file_name - string z nazwą pliku, do którego zapisujemy dane o położeniu poszczególnych punktów
 */
template <int num_of_points>
void Shape<num_of_points>::Draw(std::string file_name) const
{
  std::fstream file;
  file.open(file_name, std::ios::out);

  for (int i = 0; i < num_of_points; i++)
  {
    file << tmp_point[i] << std::endl;
    if (i % 4 == 3)
    {
      file << "#\n\n";
    }
  }
  file.close();
}

/**
 * @brief Odczytuje kształt z pliku file_name 
 * @param file_name - string z nazwą pliku, z którego czytamy dane o położeniu poszczególnych punktów
 */
template <int num_of_points>
void Shape<num_of_points>::Read(std::string file_name)
{
  std::fstream file;
  file.open(file_name);

  if (!file.is_open())
  {
    std::cerr << "Unable to load model file!"
         << std::endl;
    return;
  }

  char c = ' ';
  int i = 0;
  while (i < num_of_points)
  {
    file >> c;

    if((c > 47 && c < 58) || c == '-' || c == '+')
    {
      file.putback(c); 
      file >> point[i];
      tmp_point[i] = point[i];
      i++;
    }
  }

  file.close();
}

/**
 * @brief Przesunięcie obiektu w przestrzeni z uwzględnieniem jego aktualnego kątu obrotu
 * 
 * @tparam num_of_points liczba punktów z których składa się obiekt
 * @param change zmiana wektora translacji (przesuniecie obiektu)
 */
template <int num_of_points>
void Shape<num_of_points>::translate(const Vector3D &change)
{
  translation = translation + change;
  for (int i = 0; i < num_of_points; i++)
  {
    tmp_point[i] = tmp_point[i] + change;
  }
}

/**
 * @brief przesuniecie obiektu w przestrzeni z jego polozenia poczatkowego o wektor
 * 
 * @tparam num_of_points liczba punktow z ktorych sklada sie obiekt
 * @param change wektor o ktory chcemy przesunac obiekt
 */
template <int num_of_points>
void Shape<num_of_points>::initial_translation(const Vector3D &change)
{
  for (int i = 0; i < num_of_points; i++)
  {
    tmp_point[i] = point[i] + change ;
  }
}

/**
 * @brief metoda pozwalająca obrócić obiekt względem osi OZ
 * 
 * @tparam num_of_points liczba punktów z których składa się obiekt
 * @param ang kąt, o który chcemy obrócić obiekt
 */
template <int num_of_points>
void Shape<num_of_points>::rotateZ(const double ang)
{
  angleZ = angleZ + ang;
  Matrix<double,3> rotation;

  rotation(0,0) = std::cos(angleZ * PI/180);
  rotation(0,1) = -std::sin(angleZ * PI/180);

  rotation(1,0) = std::sin(angleZ * PI/180);
  rotation(1,1) = std::cos(angleZ * PI/180);

  rotation(2,2) = 1;
  for (int i = 0; i < num_of_points; i++)
  {
    tmp_point[i] = rotation * point[i] + translation;
  }
}

/**
 * @brief obraca obiekt wzgledem osi OZ uwzgledniajac jego aktualne polozenie
 * 
 * @tparam num_of_points liczba punktow
 * @param ang kat obrotu
 */
template <int num_of_points>
void Shape<num_of_points>::rotateZ_tmp(const double ang)
{
  angleZ = angleZ + ang;
  Matrix<double,3> rotation;

  rotation(0,0) = std::cos(angleZ * PI/180);
  rotation(0,1) = -std::sin(angleZ * PI/180);

  rotation(1,0) = std::sin(angleZ * PI/180);
  rotation(1,1) = std::cos(angleZ * PI/180);

  rotation(2,2) = 1;
  for (int i = 0; i < num_of_points; i++)
  {
    tmp_point[i] = rotation * tmp_point[i];
  }
}

/**
 * @brief metoda pozwalająca obrócić obiekt względem osi OY
 * 
 * @tparam num_of_points liczba punktów z których składa się obiekt
 * @param ang kąt, o który chcemy obrócić obiekt
 */
template <int num_of_points>
void Shape<num_of_points>::rotateY(const double ang)
{
  angleY = angleY + ang;
  Matrix<double,3> rotation;

  rotation(0,0) = std::cos(angleY * PI/180);
  rotation(0,2) = std::sin(angleY * PI/180);

  rotation(1,1) = 1;

  rotation(2,0) = -std::sin(angleY * PI/180);
  rotation(2,2) = std::cos(angleY * PI/180);

  for (int i = 0; i < num_of_points; i++)
  {
    tmp_point[i] = rotation * point[i];
  }
}

/**
 * @brief metoda pozwalająca obrócić obiekt względem osi OX
 * 
 * @tparam num_of_points liczba punktów z których składa się obiekt
 * @param ang kąt, o który chcemy obrócić obiekt
 */
template <int num_of_points>
void Shape<num_of_points>::rotateX(const double ang)
{
  angleX = angleX + ang;
  Matrix<double,3> rotation;

  rotation(0,0) = 1;

  rotation(1,1) = std::cos(angleX * PI/180);
  rotation(1,2) = -std::sin(angleX * PI/180);

  rotation(2,1) = std::sin(angleX * PI/180);
  rotation(2,2) = std::cos(angleX * PI/180);

  for (int i = 0; i < num_of_points; i++)
  {
    tmp_point[i] = rotation * point[i];
  }
}

/**
 * @brief metoda powieksza ksztalt
 * 
 * @param scale powiekszenie ksztaltu
 */
template <int num_of_points>
void Shape<num_of_points>::scale(const double scale) 
{
  for (int i = 0; i < num_of_points; i++)
  {
    point[i] = point[i] * scale;
    tmp_point[i] = point[i];
  }
}

/**
 * @brief Funkcja sprawdzajaca, czy nastapila kolizja pomiedzy obiektami
 * 
 * @tparam num_of_points liczba punkow z ktorych sklada sie obiekt
 * @param object obiekt, z ktorym porownujemy
 * @return true nastapila kolizja miedzy obiektami
 * @return false kolizja nie wystapila
 */
template <int num_of_points>
bool Shape<num_of_points>::isCollision(base_Shape object)
{
    if (object.Xmin <= this->Xmax && object.Xmax >= this->Xmin)
    {
        if (object.Ymin <= this->Ymax && object.Ymax >= this->Ymin)
        {
            if (object.Zmin <= this->Zmax && object.Zmax >= this->Zmin) return true;

        }
    }
    return false;
}
