#ifndef SVECTOR_HH
#define SVECTOR_HH

#include <iostream>
#include <iomanip>

/**
 * @brief Klasa Vector<typ,rozmiar> jest tablica dowolnego typu o dowolnym rozmiarze 
 * 
 * @tparam Type typ zmiennych wektora
 * @tparam Size ilosc danych w wektorze
 */
template <typename Type, int Size>
class Vector
{
  Type _tab[Size];
  static int all_vectors;
  static int destroyed_vectors;

public:
  /**
  * @brief Wtorzy wektor, dodaje go do ogolnej liczby wektorow oraz inicjuje go zerami
  */
  Vector()
  {
    ++all_vectors;
    for (int i = 0; i < Size; i++)
    {
      _tab[i] = 0;
    }
  }
  Vector(Vector &vec){++all_vectors;  for (int i = 0; i < Size; i++) {_tab[i] = vec(i);  } }
  ~Vector(){destroyed_vectors++;}
  /**
  * @brief Operator zapewnia dostęp do zawartosci tablicy
  */
  Type operator()(unsigned int i) const { return _tab[i]; }
  /**
* @brief Operator umozliwa zmiane zawartosci tablicy
*/
  Type &operator()(unsigned int i) { return _tab[i]; }
  /**
* @brief Zwraca łączną liczbę utworzonych wektorów
*/
  static int totalObjects(void) { return all_vectors; }
/**
* @brief Zwraca liczbę aktualnie istniejacych wektorow
*/
  static int liveObjects(void) { return all_vectors-destroyed_vectors; }
};

/**
 * @brief Przeciążenie operatora >> umożliwia wypisywanie wektora na strumień
 * 
 * @tparam Type typ danych
 * @tparam Size rozmiar wektora
 * @param stream strumień na który wypisujemy
 * @param vec wektor, który wypisujemy
 */
template <typename Type, int Size>
std::istream &operator>>(std::istream &stream, Vector<Type, Size> &vec)
{
  for (unsigned int i = 0; i < Size; i++)
  {
    stream >> vec(i);
  }
  return stream;
}

/**
 * @brief Przeciążenie operatora << pozwala zapisywać wektor.
 * 
 * @tparam Type typ danych
 * @tparam Size rozmiar wektora
 * @param stream strumień z którego czytamy dane
 * @param vec wektor, który wypełniamy danymi ze strumienia
 * @return std::ostream& 
 */
template <typename Type, int Size>
std::ostream &operator<<(std::ostream &stream, const Vector<Type, Size> &vec)
{
  for (unsigned int i = 0; i < Size; i++)
  {
    stream << vec(i) << " ";
  }
  return stream;
}

/**
 * @brief Przeciążenia operatora dodawania
 * 
 * @tparam Type 
 * @tparam Size 
 * @param vec1
 * @param vec2 
 * @return Vector<Type,Size> wektor, który jest sumą dwóch wektorów
 */
template <typename Type, int Size>
Vector<Type, Size> operator+(const Vector<Type, Size> &vec1, const Vector<Type, Size> &vec2)
{
  Vector<Type, Size> result;

  for (unsigned int i = 0; i < Size; i++)
  {
    result(i) = vec1(i) + vec2(i);
  }
  return result;
}

/**
 * @brief Przeciążenie operatora odejmowania
 * 
 * @tparam Type 
 * @tparam Size 
 * @param vec1 
 * @param vec2 
 * @return Vector<Type,Size> wektor, który jest różnicą dwóch wektorów
 */
template <typename Type, int Size>
Vector<Type, Size> operator-(const Vector<Type, Size> &vec1, const Vector<Type, Size> &vec2)
{
  Vector<Type, Size> result;

  for (int i = 0; i < Size; i++)
  {
    result(i) = vec1(i) - vec2(i);
  }
  return result;
}

/**
 * @brief Przeciążenie operatora mnożenia wektorów
 * 
 * @tparam Type 
 * @tparam Size 
 * @param vec1 
 * @param vec2 
 * @return Type Zwraca wartość, o typie z którego zbudowane są wektory
 */
template <typename Type, int Size>
Type operator*(const Vector<Type, Size> &vec1, const Vector<Type, Size> &vec2)
{
  Type result(0);

  for (int i = 0; i < Size; i++)
  {
    result += vec1(i) * vec2(i);
  }
  return result;
}

/**
 * @brief Przeciążenie operatora * dla mnożenia wektora przez liczbe
 * 
 * @tparam Type 
 * @tparam Size 
 * @param vec 
 * @param t 
 * @return Vector<Type,Size> zwraca wektor przemnoznony przez liczbe
 */
template <typename Type, int Size>
Vector<Type, Size> operator*(const Vector<Type, Size> &vec, const float t)
{
  Vector<Type, Size> result;

  for (int i = 0; i < Size; i++)
  {
    result(i) = vec(i) * t;
  }
  return result;
}

/**
 * @brief Przeciążenie operatora /
 * 
 * @tparam Type 
 * @tparam Size 
 * @param vec 
 * @param t 
 * @return Vector<Type,Size> zwraca wektor podzielony przez liczbe t
 */
template <typename Type, int Size>
Vector<Type, Size> operator/(const Vector<Type, Size> &vec, const double t)
{
  Vector<Type, Size> result;

  for (int i = 0; i < Size; i++)
  {
    result(i) = vec(i) / t;
  }
  return result;
}

#endif